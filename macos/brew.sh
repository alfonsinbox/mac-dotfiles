
# install homebrew https://brew.sh
echo "installing homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install utilites
echo "brew installing utilities"
brew install git \
  tree \
  youtube-dl \
  node \
  ffmpeg \
  pyenv \
  zsh \
  z \
  wget

# Install programs
echo "brew installing programs"
brew cask install google-chrome \
  google-cloud-sdk \
  firefox \
  intellij-idea \
  docker \
  spotify \
  minikube \
  discord \
  balenaetcher \
  visual-studio-code \
  vlc \
  skype 

echo "cleaning up brew packages"
brew cleanup