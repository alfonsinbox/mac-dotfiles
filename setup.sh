#!/usr/bin/env bash

# ~/.macos — https://mths.be/macos
# Modified by Kent C. Dodds
# Run without downloading:
# curl https://gitlab.com/alfonsinbox/mac-dotfiles/raw/master/setup.sh | bash

ssh-keygen -t ed25519
pbcopy < ~/.ssh/id_ed25519.pub
echo "  IdentityFile ~/.ssh/id_ed25519" >> ~/.ssh/config
echo "Your SSH key has been copied to clipboard."
echo "Add it to your Gitlab account"
echo "Press [enter] when done "
read

# Link dotfiles
echo "cloning dotfiles"
git clone git@gitlab:alfonslange/dotfiles.git "${HOME}/Git/dotfiles"
ln -s "${HOME}/Git/dotfiles/.zshrc" "${HOME}/.zshrc"
# ln -sf "${HOME}/Git/dotfiles/.ssh/config" "${HOME}/.ssh/config"

chmod +x macos/*.sh

./macos/brew.sh
./macos/preferences.sh
./macos/xcode-install.sh
./macos/zsh-install.sh
